<?php
/**
 * Created by: Stephan Hoeksema 2021
 * bc-php
 */

class Strings
{
    //public protected private
    public $name;
    public $yos; //year of start
    public $school;

    /**
     * Strings constructor.
     * @param $name name of the study
     * @param $yos year started at name ;-)
     * @param $school name of the school.
     */
    public function __construct($name, $yos, $school)
    {
        $this->name = $name;
        $this->yos = $yos;
        $this->school = $school;
    }

    /**
     * @return name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param name $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



}