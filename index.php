<?php
require 'classes/Strings.php';
/**
 * PHP ondersteund de volgende data types:
 * String
 * Integer
 * Float (floating point numbers - also called double)
 * Boolean
 * Array
 * Object
 * NULL
 * Resource
 *
 * Variabelen in PHP zijn case sensitive ($a is niet $A)
 *
 * Om code 'uit' te zetten gebruiken we comments // of /* */
/**
 * PHP heeft veel built in functions die we gebruiken tijdens deze
 * code sessies zal ik verschillende laten zien en in de opdrachten
 * wordt er ook gevraagd om andere built-in functions te gebruiken.
 */

/** String
 * Een string is een serie characters zoals 'Hallo ADSD 2021'.
 * De string kan worden omringd met een single(') of double(") quotes.
 * Het verschil tussen deze twee moet je met programmeren zien!
 */
echo "<hr><h1>STRING</h1><hr>";
/** string voorbeeld */
$adsd = new Strings('ADSD 2021-2022', 2021, 'Hogeschool Windesheim');
echo "Welkom " . $adsd->getName() ."!";

echo "<br>";
// built in function strlen() string length
/** Opdracht strings
 * 1. Maak een class Student
 * 2. Maak een public variabele waarin je naam zet. |Stephan <jouw naam>|
 * 3. Maak een public variabele met je geboortestad. |Amsterdam <jouw geboortestad>|
 * 4. Laat je naam en geboortestad op het scherm zien. |Stephan is geboren in Amsterdam|
 * 5. Zoek 4 inline functie voor strings en laat daarbij voorbeelden zien. |https://www.php.net/manual/en/ref.strings.php|
 */
